from django.db import models
from datetime import datetime, date

# Create your models here.

class Empleado(models.Model):
	apellidos = models.CharField(max_length=100)
	nombres = models.CharField(max_length=100)
	correo_electronico = models.CharField(max_length=100)

class Tarea(models.Model):
	nombre = models.CharField(max_length=100)
	descripcion = models.TextField()
	fecha_limite = models.DateField(default=date.today)
	terminada = models.BooleanField(default=False)
	empleado = models.ForeignKey(Empleado, on_delete=models.DO_NOTHING)


	


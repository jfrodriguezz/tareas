# Generated by Django 3.0.5 on 2022-04-29 14:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20220429_1134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tarea',
            name='fecha_limite',
            field=models.DateField(null=True),
        ),
    ]

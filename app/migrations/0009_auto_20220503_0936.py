# Generated by Django 3.0.5 on 2022-05-03 12:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20220502_0755'),
    ]

    operations = [
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('apellidos', models.CharField(max_length=100)),
                ('nombres', models.CharField(max_length=100)),
                ('correo_electronico', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='tarea',
            name='empleado',
            field=models.OneToOneField(default=0, on_delete=django.db.models.deletion.CASCADE, to='app.Empleado'),
        ),
    ]

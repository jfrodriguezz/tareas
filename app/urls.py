from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from .views import *
from . import views

urlpatterns = [
    path('', index, name='index'),

    path('tareas', tareas, name='tareas'),
    
    path('tareas/crear', tareas_crear, name='tareas_crear'),
    path('tareas/guardar', tareas_guardar, name='tareas_guardar'),

    path('tareas/editar/<int:myid>/', tareas_editar, name='tareas_editar'),
    path('tareas/actualizar/<int:myid>/', tareas_actualizar, name='tareas_actualizar'),

    path('tareas/terminar/<int:myid>/', tareas_terminar, name='tareas_terminar'),

    path('tareas/eliminar/<int:myid>/', tareas_eliminar, name='tareas_eliminar'),

    # path('empleados/pdf', empleados_pdf, name='empleados_pdf'),

    path('reporte', views.TareasPdf.as_view(), name='reporte'),

    path('empleados', empleados, name='empleados'),
    path('empleados/cantidad_tareas/<int:myid>/', cantidad_tareas, name='cantidad_tareas'),
]

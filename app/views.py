from django.shortcuts import render
from .models import *
import datetime
from django.utils.html import escape
from django.http import HttpResponse
from django.shortcuts import redirect
from django.http import FileResponse
import io 
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import letter

from django.views.generic import ListView, View

from django.template.loader import get_template
from xhtml2pdf import pisa
from io import BytesIO
from django.db.models import Count 

# Create your views here.

# VISTA INDEX TAREAS
def index(request):
    return render(request, "index.html")

# VISTA INDEX TAREA
def tareas(request):
    tareas = Tarea.objects.all()
    context = {
        'tareas' : tareas,
    }
    return render(request, 'tareas.html', context)

# VISTA CREAR TAREA
def tareas_crear(request):
    empleados = Empleado.objects.all()
    context = {
        'empleados' : empleados
    }
    return render(request, "tareas_crear.html", context)

# METODO PARA GUARDAR UNA TAREA
def tareas_guardar(request):
    nombre = request.POST.get('nombre')
    descripcion = request.POST.get('descripcion')
    fecha_limite = request.POST.get('fecha_limite')
    empleado_id = request.POST.get('empleado')
    empleado = Empleado.objects.get(id=empleado_id)
    Tarea.objects.create(
        nombre = nombre,
        descripcion = descripcion,
        fecha_limite = fecha_limite,
        empleado = empleado
    )
    
    return redirect (tareas)

# VISTA EDITAR TAREA
def tareas_editar(request, myid):
    tarea = Tarea.objects.get(id = myid)
    empleados = Empleado.objects.all()
    context = {
        'tarea' : tarea,
        'empleados' : empleados
    }
    return render(request, "tareas_editar.html", context)

# METODO PARA ACTUALIZAR UNA TAREA
def tareas_actualizar(request, myid):
    nombre = request.POST.get('nombre')
    descripcion = request.POST.get('descripcion')
    fecha_limite = request.POST.get('fecha_limite')
    empleado_id = request.POST.get('empleado')
    empleado = Empleado.objects.get(id=empleado_id)

    Tarea.objects.filter(pk=myid).update(
        nombre = nombre,
        descripcion = descripcion,
        fecha_limite = fecha_limite,
        empleado = empleado
    )

    return redirect (tareas)

# METODO PARA TERMINAR UNA TAREA
def tareas_terminar(request, myid):
    Tarea.objects.filter(pk=myid).update(terminada=True)
    return redirect (tareas)

# METODO PARA ELIMINAR UNA TAREA

def tareas_eliminar(request, myid):
    Tarea.objects.filter(pk=myid).delete()
    return redirect (tareas)

"""
def empleados_pdf(request):
    buf = io.BytesIO()
    c = canvas.Canvas(buf, pagesize=letter, bottomup=0)
    textob = c.beginText()
    textob.setTextOrigin(inch, inch)
    textob.setFont("Helvetica", 14)
    
    empleados = Empleado.objects.all()

    lines = []

    for empleado in empleados:
        lines.append(empleado.apellidos)
        lines.append(empleado.nombres)
        lines.append(" ")

    for line in lines:
        textob.textLine(line)
    
    c.drawText(textob)
    c.showPage()
    c.save()
    buf.seek(0)
    return FileResponse(buf, as_attachment=True, filename='empleados.pdf')
"""

def reporte(request):
    tareas = Tarea.objects.all()
    context = {
        'tareas' : tareas,
    }
    return render(request, 'reporte_tabla.html', context)

def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(),  content_type='application/pdf')
    return None

class TareasPdf(View):
    def get(self, request, *args, **kwargs):
        tareas = Tarea.objects.all()
        data = {
            'tareas' : tareas
        }
        pdf = render_to_pdf('reporte_tabla.html', data)
        return HttpResponse(pdf, content_type='application/pdf')

#empleados
def empleados(request):
    empleados = Empleado.objects.all()
    cantidad_empleados = Empleado.objects.count()

    context = {
        'empleados' : empleados,
        'cantidad_empleados' : cantidad_empleados,
    }
    return render(request, 'empleados.html', context)

def cantidad_tareas(request, myid): 
    cantidad_tareas = Tarea.objects.filter(empleado=myid).count() 
    return render(request, 'cantidad_tareas.html', {'cantidad_tareas': cantidad_tareas})




